# syntax=docker/dockerfile:1
FROM alpine:3.16

RUN apk add --no-cache openconnect
ADD entrypoint.sh /entrypoint.sh

HEALTHCHECK --interval=10s --timeout=10s --start-period=10s \
    CMD /sbin/ifconfig tun0

CMD ["/entrypoint.sh"]